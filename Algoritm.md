### 9. Data structure and algoritms

- **What are the differences between Array and linkedList?**

- **What are the differences between Array and ArrayList?**

- **Find duplicate an item in a non-sorted list?**
    
   Solution 1: The method add of Set returns a boolean whether a value already exists (**true** if it does *not exist*, **false** if it already *exists*).
        
        ```
        public Set<Integer> findDuplicates(List<Integer> listContainingDuplicates)
            { 
              final Set<Integer> setToReturn = new HashSet<>(); 
              final Set<Integer> set1 = new HashSet<>();
            
              for (Integer yourInt : listContainingDuplicates)
              {
               if (!set1.add(yourInt))
               {
                setToReturn.add(yourInt);
               }
              }
              return setToReturn;
            }
        ```
        
    Solution 2: We can use HashMap to store the repeat number of each item in the list too.
    
    
- **How to implement a stack using queue?**
    
    A Stack is a subclass of Vector class and it represents last-in-first-out (LIFO) stack of objects. 
    The last element added at the top of the stack (In) can be the first element to be removed (Out) from the stack.
    A Queue class extends Collection interface and it supports the insert and removes operations using a first-in-first-out (FIFO). 
    We can also implement a Stack using Queue in the below program.
    

```
    public class StackFromQueueTest {
       Queue queue = new LinkedList();
       public void push(int value) {
          int queueSize = queue.size();
          queue.add(value);
          for (int i = 0; i < queueSize; i++) {
             queue.add(queue.remove());
          }
       }
       public void pop() {
          System.out.println("An element removed from a stack is: " + queue.remove());
       }
       public static void main(String[] args) {
          StackFromQueueTest test = new StackFromQueueTest();
          test.push(10);
          test.push(20);
          test.push(30);
          test.push(40);
          System.out.println(test.queue);
          test.pop();
          System.out.println(test.queue);
       }
    }    
```

**The Output**
    ```
    [40, 30, 20, 10]
    An element removed from a stack is: 40
    [30, 20, 10]
    ```

- **How do you find the largest and smallest number in an unsorted integer array?**
- **Given an array of size n with range of numbers from 1 to n+1. The array doesn’t contain any duplicate, one number is missing, find the missing number.**
- **A sorted array is rotated at some unknown point, how to efficiently search an element in it.**
- **How to find if two given rectangles overlap?**
- **How to swap two integers without swapping the temporary variable in Java?**
- **How do you check if a string contains only digits?**
- **How to sort a list?**