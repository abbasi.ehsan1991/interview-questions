### 7. Gradle

- **What is BuildVariant, BuildType, Falvour?**
    
    Complete Informationhttps: (https://developer.android.com/studio/build/build-variants)

- **What do you do if you want to publish different versions of an APK with the same codabase?**

    *using product flavor.* [What is flavor?] (https://android.jlelse.eu/product-flavors-for-android-library-d3b2d240fca2)

- **How to add a dependency only on a certain build of the app?**

  Flavor dependency. What? don't worry, [read this link] (https://developer.android.com/studio/build/dependencies#dependency-configurations)

- **What is the difference between `implementation` and `api`?**

  These two keywords work the same when you want to add a new library but the main difference occurs when using it in the internal library.
  Let's explain it with an example. Consider your app has a library called 'libraryA'. 
  This library is also dependant on another library called 'libraryB'. the dependency flow will be : `app -> libraryA -> libraryB` . 
  If the libraryB is declared in libraryA with keyword `implementation`, so your app module does not know anything about the classes of libraryB.
  So you can't access and use any classes of libraryB. If you want to use of libraryB classes in your app too, you must declare libraryB in the libraryA Gradle file with keyword `api`. 
  For more information read [this medium link](https://medium.com/mindorks/implementation-vs-api-in-gradle-3-0-494c817a6fa).


- **What do you mean by Gradle wrapper?**

  The Gradle wrapper is the most suitable way to initiate a Gradle build. A Gradle wrapper is a Window�s batch script which has a shell script for the OS (operating system). Once you start the Gradle build via the wrapper, you will see an auto download which runs the build.
  [More info](http://kevinpelgrims.com/blog/2015/05/25/use-the-gradle-wrapper-for-your-android-projects/)
