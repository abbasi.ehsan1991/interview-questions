### 2. JAVA

- **How to prevent a class to be extended?**

  simply use keyword `final` in definition of class or methods. for example:
  ```java
  final public class CantOverrideClass {

    public final void cantOverrideMethod(){

    }

  }
  ```  

- **What is the use of the finalize method?**

  `finalize()` method is a protected and non-static method of java.lang.Object
  class. This method will be available in all objects you create in java. This
  method is used to perform some final operations or clean up operations on an
  object before it is removed from the memory. you can override the `finalize()`
  method to keep those operations you want to perform before an object is
  destroyed. Here is the general form of `finalize()` method.

  ```java
  protected void finalize() throws Throwable {
    //Keep some resource closing operations here
  }
  ```

- **What is a static variables in Java?**

  static is a non-access modifier in Java which is applicable for the following:
    - blocks
    - variables
    - methods
    - nested classes

  When a variable is declared as static, then a single copy of variable is created and shared among all objects at class level. Static variables are, essentially, global variables. All instances of the class share the same static variable.

  Important points for static variables:
    - We can create static variables at class-level only. See [here](https://www.geeksforgeeks.org/g-fact-47/)
    - static block and static variables are executed in order they are present in a program.

- **OverLoading for static method, possible?**
 

  We have two kind of methods in Java:
   *  Instance(Not static) methods
   * Class(Static) methods
  

  *Actually Static methods belons to Classes not to Instance of a class*

  So, The answer is ***Yes***. *We* can have two ore more static methods with same name, but differences in input parameters. For example, consider the following Java program.

```java
public class Test { 
        public static void foo() { 
            System.out.println("Test.foo() called "); 
        } 
        public static void foo(int a) {  
            System.out.println("Test.foo(int) called "); 
        } 
        public static void main(String args[]) 
        {  
            Test.foo(); 
            Test.foo(10); 
        } 
} 
```

    
    
Can we overload methods that differ only by static keyword?
We cannot overload two methods in Java if they differ only by static keyword (number of parameters and types of parameters is same).    
Can we Override static methods in java?
We can declare static methods with same signature in subclass, but it is not considered overriding as there won’t be any run-time polymorphism.     Hence the answer is ‘***No***’.
  
If a ***derived*** class defines a static method with same signature as a static method in ***base*** class, the method in the ***derived*** class   the method in the ***base*** class.
    
    
    [Complete Article](https://www.geeksforgeeks.org/can-we-overload-or-override-static-methods-in-java/)
        


- **What is an abstract class? Benefits?**

  According to the official document, An abstract class is a class that is declared `abstract`. It may or may not include abstract methods. Abstract classes cannot be instantiated, but they can be subclassed. Also, An abstract method is a method that is declared without an implementation (without braces, and followed by a semicolon), If a class includes abstract methods, then the class itself must be declared `abstract`.
  ```java
  public abstract class GraphicObject {
    // declare fields
    // declare nonabstract methods
    abstract void draw();
  }
  ```
  We use abstraction when we want to enforce base functions a have base
  properties. Although we could use an interface for this, sometimes the
  functionality of such classes may overlap or it needs some objects which
  are shared in whole class scope, then we use abstraction.

- **What is an object cloning? can you use clone() method of every object ?**

  Object cloning refers to creation of exact copy of an object. It creates a new instance of the class of current object and initializes all its fields with exactly the contents of the corresponding fields of this object. Every class that implements `clone()` methods should call super.clone() to obtain the cloned object reference. Also it must implement java.lang.Cloneable interface otherwise it will throw CloneNotSupportedException when clone method is called on that class’s object.
  ```java
  protected Object clone() throws CloneNotSupportedException
  ```

- **Difference between == and .equals() method in Java**

    In general both “equals()“ and “==” operator in Java are used to compare objects to check equality but here are some of the differences between the two:
    Main difference between “.equals()“ method and “==“ operator is that one is method and other is operator. 
    We can use “==“ operators for reference comparison (address comparison) and .equals() method for content comparison. In simple words, == checks if both objects point to the same memory location whereas .equals() evaluates to the comparison of values in the objects.

- **Multiple inheritances? Possible? How can we do that?**

  Multiple inheritance in Java programming is achieved or implemented using interfaces. Java does not support multiple inheritance using    classes. In simple term, a class can inherit only one class and multiple interfaces in a java programs.
Java doesn’t allow multiple inheritance to avoid the ambiguity caused by it. One of the example of such problem is the diamond problem that occurs in multiple inheritance.
  [Complete Article](https://dzone.com/articles/dealing-with-diamond-problem-in-java)


- **Object scopes?**

  `public` , `protected` , default (no modifier) , `private`
	Class	Package	Subclass
(same pkg)	Subclass
(diff pkg)	World
public	+	+	+	+	+
protected	+	+	+	+	
no modifier	+	+	+		
private	+				


- **Override private methods, possible?**

  No, a private method cannot be overridden since it is not visible from any other class.

- **why access to the non-static variable is not allowed from static method in Java?**

  because non-static variable are associated with a specific instance of an object while static is not associated with any instance.

- **Java reference types?** [*geeksforgeeks*]("https://www.geeksforgeeks.org/types-references-java/")

- **What is Generic in Java?**

  Generics enable types (classes and interfaces) to be parameters when defining classes, interfaces and methods. Type parameters provide a way for you to re-use the same code with different inputs. The difference is that the inputs to formal parameters are values, while the inputs to type parameters are types. [more details](https://www.geeksforgeeks.org/generics-in-java/)

- **What is the difference between int and Integer?**

  `int` is a primitive type, while `Integer` is class with a single field of type `int`. Variables of type `int` store the avtual binary value for the integer. Variables of type `Integer` store references to `Integer` objects, just as with any other reference (object) type. The `Integer` class is used where you need an `int` to be treated like any other object, such as in generic types or situations where you need nullability.

- **What are Autoboxing and unboxing?**

  - **Autoboxing:** Converting a primitive value into an object of the corresponding wrapper class is called autoboxing. For example, converting `int` to `Integer` class.
  - **Unboxing:**  Converting an object of a wrapper type to its corresponding primitive value is called unboxing. For example conversion of `Integer` to `int`.

- **What is the difference between initialization and instantiation?**

  - **Instantiation** is when memory is allocated for an object. This is what the `new` keyword is doing. A reference to the object that was created is returned from the `new` keyword.

  - **Initialization** is when values are put into the memory that was allocated. This is what the Constructor of a class does when using the `new` keyword. A variable must also be initialized by having the reference to some object in memory passed to it.


- **How does a static block work?**

  Run once when class is loaded, used for initializing static members. [read more](https://www.geeksforgeeks.org/g-fact-79/)

- **What does the keyword `synchronized` mean?**

  A `synchronized` block in Java is synchronized on some object. All synchronized blocks synchronized on the same object can only have one thread executing inside them at a time. All other threads attempting to enter the synchronized block are blocked until the thread inside the synchronized block exits the block.

- **What is the memory leak? How to handle it?**
- **What is `transient` modifier? What does it come for?** [complete explanation](https://www.geeksforgeeks.org/transient-keyword-java/)

- **What is the difference between `==` and `.equal`?**
  - The `equals()` method compares two strings, character by character, to determine equality.
  - The `==` operator checks to see whether two object references refer to the same instance of an object

- **What is `reflection`?** [geeksforgeeks](https://www.geeksforgeeks.org/reflection-in-java/)
- **What is the `volatile` modifier?**

  In multi-threading apps where the threads operate on non-volatile variables, each thread may copy variables from main memory into a CPU cache, for performance reason. If your app run on more than one thread, each thread may copy the variable and cache it. So This keyword is used to mark a variable as "being stored in main memory". Note that reading from and writing to main memory is more expensive than accessing the CPU cache. Thus, you should only use volatile variables when you really need to enforce visibility of variables.

  ![](/assets/images/volatile-preview.png)

- **What is the `hashCode()` used for?**

  `hashcode()` returns the hashcode value as an Integer. Hashcode value is mostly used in hashing based collections like HashMap, HashSet, HashTable….etc. According to the official documentation, The general contract of `hashCode()` is:
    - Whenever it is invoked on the same object more than once during an execution of a Java application, the hashCode method must consistently return the same integer, provided no information used in equals comparisons on the object is modified. This integer need not remain consistent from one execution of an application to another execution of the same application.
    - If two objects are equal according to the equals(Object) method, then calling the hashCode method on each of the two objects must produce the same integer result.

    - It is not required that if two objects are unequal according to the equals(java.lang.Object) method, then calling the hashCode method on each of the two objects must produce distinct integer results. However, the programmer should be aware that producing distinct integer results for unequal objects may improve the performance of hashtables.


- **What are "annotations"?**

  Java annotations are used to provide meta data for your Java code. Being meta data, Java annotations do not directly affect the execution of your code, although some types of annotations can actually be used for that purpose. [read more](http://tutorials.jenkov.com/java/annotations.html)

- **What is thread-safe mean? How we can make our code thread-safe?**

  Thread safety in java is the process to make our program safe to use in multithreaded environment, there are different ways through which we can make our program thread safe.
    - Synchronization
    - Use of Atomic Wrapper, For example AtomicInteger.
    - Use of locks from java.util.concurrent.locks package.
    - Using thread safe collection classes
    - Using volatile keyword.

  Note that if two threads are both reading and writing to a shared variable, then using the volatile keyword for that is not enough. You need to use a synchronized in that case to guarantee that the reading and writing of the variable is atomic. Reading or writing a volatile variable does not block threads reading or writing. For this to happen you must use the synchronized keyword around critical sections.

- **what is the difference between `throw` and `throws`?**

  Keyword `throw` is used to explicitly throw as an exception in the body of function, while `throws` is utilized to handle checked exceptions for re-intimating the compiler that exceptions are being handled. The throws need to be used in the function’s signature and also while invoking the method that raises checked exceptions.

<br>

