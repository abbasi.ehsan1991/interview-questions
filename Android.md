### 4. Android

- **What is `Application` class?**

- **Difference between `Activity` and `Service`?**
- **Why do android apps need to ask permission like `INTERNET` or `LOCATION`?**
- **Differences between `serializable` and `Parcalable`?**
- **Why `serializable` body is empty? How is it doing?**
- **Which method in `fragment` runs only once?**
- **How does the activity respond when orientation is changed?**
- **How to know `configChange` happens in `onDestroy()` function?**
- **Explain Activity launch modes?**
    1.  Standard
    2.  SingleTop
    3.  SingleTask
    4.  SingleInstance
    
**Standard:**
This is the default launch mode of activity.It creates a new instance of activity every time even if activity instance is already present.
Suppose we have A, B, C, and D activities and your activity B has standard launch mode. Now again launching activity B

State of Activity Stack before launch B

A →B→ C→D

State of Activity Stack after launch B

A → B → C→D→ B

We can see that new instance of B is created again.

**SingleTop:**
If an instance of activity already exists at the top of the current task, a new instance will not be created and the Android system will 
Route the intent information through **onNewIntent()**.
If an instance is **not** present on **top** of the task then a new instance will be created.

Suppose we have A, B, C, and D activities. A →B →C →D

If we launch C then a new instance of C will be created as it is not on top.
So it will look like A →B →C →D →C

Now suppose we have A →B →C →D →C like this
then we if again launch C activity then in this case new instance will not be created. Instead, we will receive the callback on onNewIntent() method.

**SingleTask:**
An activity declared with launch mode as singleTask can have only one instance in the system (singleton). At a time only one instance of activity will exist.

If activity instance is not present then the new instance will be created and if the instance is already present in the system then the onNewIntent() method will receive the callback.

Suppose we have A, B, C activities(A →B →C ) and we are launching D that has a singleTask launch mode. In that case, the new instance of D will be created so the current state will look like this. (A →B →C →D)

Now let suppose if we launch B that also have has a singleTask launch mode then current state will look like this.

A →B

Here old instance gets called and intent data route through onNewIntent() callback. Also, notice that C and D activities get destroyed here.

**SingleInstance:**

It is similar to singleTask except that no other activities will be created in the same task. If another Activity is called from this kind of Activity, a new Task would be automatically created to place that new Activity.
Case 1:

Suppose you have A, B, and C activities(A →B →C) and your activity D has a singleInstance launch mode. In this case, if we launch D then D will be launch in the diffrent task. New task for D will be created.

Task1: A →B →C

Task2 : D (here D will be in the different task)

Now if you continue this and start E and D then Stack will look like

Task1: A →B →C →E

Task2: D
Case 2:

Suppose you have A, B, C activities in one task (A →B →C)and activity D is in another task with launch mode singleInstance. In this case, if we launch D again then we will receive the callback in the onNewIntent() method of D activity.

Task1: A →B →C

Task2: D (Here old instance gets called and intent data route through onNewIntent() callback)

(https://medium.com/mindorks/android-launch-mode-787d28952959 "More complete Info about Activity Launch modes")


- **How to prevent the data from reloading when orientation is changed?**

  The most basic approach would be to use a combination of `ViewModels` and `onSaveInstanceState()`. A `ViewModel` is LifeCycle-Aware. In other words,
  a `ViewModel` will not be destroyed if its owner is destroyed for a
  configuration change (e.g. rotation). The new instance of the owner will
  just re-connected to the existing `ViewModel`. So if you rotate an `Activity`
  three times, you have just created three different `Activity` instances, but
  you only have one `ViewModel`. So the common practice is to store data in the
  `ViewModel` class (since it persists data during configuration changes) and
  use `OnSaveInstanceState()` to store small amounts of UI data.

- **How to handle multiple screen sizes?**
- **What is the difference between margin and padding?**

   - **Padding** will be space added inside the container, for instance,
    if it is a button, padding will be added inside the button.       

  - **Margin** will be space added outside the container.

- **What is `sw` keyword in `layout-sw600` folder meaning?**
    smallestWidth - sw<N>dp - The smallestWidth is a fixed screen size characteristic of the device; the device's smallestWidth does not change when the screen's orientation changes.
    Available width - w<N>dp - This configuration value will change when the orientation changes between landscape and portrait to match the current actual width.
    Example. Say that you have a device that is 600dp x 400dp.
        If you have a w600dp resource, it will be used in landscape, but not in portrait.
        If you have a sw600dp resource, it will not be used for any orientation (smallest is 400).
- **What is the difference between `sw` and `w` and `h` as postfix in order to define the resources folder?**
- **How RecyclerView works? Explain about the functions, How many time they calls and so on.**

[Complete article about RecyclerView Story.](https://medium.com/@nileshsingh/understanding-recyclerview-part-1-the-basics-a7bd07cfae93)

- **What are the major differences between `ListView` and `RecyclerView`?**
  - **ViewHolder Pattern**: `Recyclerview` implements the ViewHolders pattern
    whereas it is not mandatory in a ListView. A `ViewHolder` object stores
    each of the component views inside the tag field of the Layout, so you can
    immediately access them without the need to look them up repeatedly.
    In `ListView`, the code might call `findViewById()` frequently during the
    scrolling of `ListView`, which can slow down performance. Even when the
    `Adapter` returns an inflated view for recycling, you still need to look up
    the elements and update them. A way around repeated use of `findViewById()`
     is to use the "view holder" design pattern.

  - **LayoutManager**: In a `ListView`, the only type of view available is
    the `vertical` ListView. A `RecyclerView` decouples list from its container
    so we can put list items easily at run time in the different containers
    (linearLayout, gridLayout) by setting LayoutManager.

  - **Item Animator**: `ListViews` are lacking in support of good animations,
    but the `RecyclerView` brings a whole new dimension to it.

- **Difference between `Intent` and `IntentService`?**
  - `Service` is the base class for Android services that can be extended to
    create any service. A class that directly extends `Service` runs on the main
    thread so it will block the UI (if there is one) and should therefore either
    be used only for short tasks or should make use of other threads for longer
    tasks.

  - `IntentService` is a subclass of `Service` that handles asynchronous requests
   (expressed as `Intents`) on demand. Clients send requests through
   `startService(Intent)` calls. The service is started as needed, handles each
   `Intent` in turn using a worker thread, and stops itself when it runs out of
   work. [Read More on Mindorks's blog]("https://blog.mindorks.com/service-vs-intentservice-in-android")

- **How to pass items to `fragment`?**
- **What is `Fragment`?**
- **How would you communicate between two `fragments`?**
- **Difference between adding/replacing `fragment` in `backstack`?**
  - `replace` removes the existing `fragment` and adds a new `fragment`.
    This means when you press back button the fragment that got replaced will
    be created with its onCreateView being invoked.

  - `add` retains the existing fragments and adds a new `fragment` that means
    existing fragment  will be active and they wont be in 'paused' state hence
    when a back button is pressed onCreateView is not called for the existing
    fragment(the fragment which was there before new fragment was added).

    In terms of fragment’s life cycle events `onPause()`, `onResume()`,
    `onCreateView()` and other life cycle events will be invoked in case of
    `replace` but they wont be invoked in case of `add`.

- **What is the difference between `dialog` and `dialogFragment`?**

- **What is the difference between `Thread` and `AsyncTask`?**

- **What is the relationship between the life cycle of an `AsyncTask` and an `Activity`? What problems can this result in? How can these problems be avoided?**

  An AsyncTask is not tied to the life cycle of the Activity that contains it.
  So, for example, if you start an AsyncTask inside an Activity and the user
  rotates the device, the Activity will be destroyed (and a new Activity
  instance will be created) but the AsyncTask will not die but instead goes
  on living until it completes.

  Then, when the AsyncTask does complete, rather than updating the UI of the
  new Activity, it updates the former instance of the Activity (i.e., the one
  in which it was created but that is not displayed anymore!). This can lead to
  an Exception (of the type java.lang.IllegalArgumentException: View not attached
  to window manager if you use, for instance, findViewById to retrieve a view
  inside the Activity).

  There’s also the potential for this to result in a memory leak since the
  AsyncTask maintains a reference to the Activity, which prevents the Activity
  from being garbage collected as long as the AsyncTask remains alive.

  For these reasons, using AsyncTasks for long-running background tasks is
  generally a bad idea . Rather, for long-running background tasks, a different
  mechanism (such as a service) should be employed.

- **What is a Handler, Looper, HandlerThread?**

    Everything(https://blog.mindorks.com/android-core-looper-handler-and-handlerthread-bd54d69fe91a)
    RelatedVideos(https://www.youtube.com/playlist?list=PL6nth5sRD25hVezlyqlBO9dafKMc5fAU2)

- **What is `Lopper` and how it works?**

**What is Looper?**

Looper is a class which is used to execute the Messages(Runnables) in a queue. Normal threads have no such queue, e.g. simple thread does not have any queue. It executes once and after method execution finishes, the thread will not run another Message(Runnable).

Where we can use Looper class?

If someone wants to execute multiple messages(Runnables) then he should use the Looper class which is responsible for creating a queue in the thread. For example, while writing an application that downloads files from the internet, we can use Looper class to put files to be downloaded in the queue.

How it works?

There is prepare() method to prepare the Looper. Then you can use loop() method to create a message loop in the current thread and now your Looper is ready to execute the requests in the queue until you quit the loop.

Here is the code by which you can prepare the Looper.

```
class LooperThread extends Thread {
      public Handler mHandler;

      @Override
      public void run() {
          Looper.prepare();

          mHandler = new Handler() {
              @Override
              public void handleMessage(Message msg) {
                  // process incoming messages here
              }
          };

          Looper.loop();
      }
  }
```


- **What are Handlers?**
    A Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue. 
    Each Handler instance is associated with a single thread and that thread's message queue. 
    When you create a new Handler it is bound to a Looper. 
    It will deliver messages and runnables to that Looper's message queue and execute them on that Looper's thread.
    1-  There are two main uses for a Handler: (1) to schedule messages and runnables to be executed at some point in the future; 
    2-  and (2) to enqueue an action to be performed on a different thread than your own.
    Handlers are objects for managing threads. It receives messages and writes
    code on how to handle the message. They run outside of the activity’s
    lifecycle, so they need to be cleaned up properly or else you will have
    thread leaks. Handlers allow communicating between the background thread
    and the main thread.

- **What is the difference between `Foreground` and `Background` and `Bounded` service?**
  - __Foreground Service:__ A foreground `service` performs some operation that
  is noticeable to the user. For example, we can use a foreground service to
  play an audio track. A `Notification` must be displayed to the user.

  - __Background Service:__ A background `service` performs an operation that
  isn’t directly noticed by the user. In Android API level 26 and above, there
  are restrictions to using background services and it is recommended to use
  WorkManager in these cases

  - __Bound Service:__ A `service` is bound when an application component binds
  to it by calling `bindService()`. A bound service offers a client-server
  interface that allows components to interact with the `service`, send requests,
  receive results. A bound service runs only as long as another application
  component is bound to it. [Read More](https://developer.android.com/guide/components/services)

- **What are the limitations of using `Services` in android 8 and higher?**
- **What is `JobScheduling`?**
- **What is `contentProvider` and what is typically used for?**

  A `ContentProvider` provides data from one application to another, when
  requested. It manages access to a structured set of data. It provides mechanisms for defining data security. [Learn more]("https://medium.com/@sanjeevy133/an-idiots-guide-to-android-content-providers-part-1-970cba5d7b42" "An idiot guide to android content providers").
  For further reading see the [official android documentation]("https://developer.android.com/guide/topics/providers/content-provider-basics" "Android official documentation")

  ![Conent Provider diagram](/assets/images/content-provider-diagram.png)

- **What is the difference between `apply()` and `commit()` in `sharedPreferences`?**
  - `commit()` writes the data **synchronously** and returns a boolean value of
    success or failure depending on the result immediately.

  - `apply()` is **asynchronous** and it won’t return any boolean response. Also
    if there is an `apply()` outstanding and we perform another `commit()`,
    The `commit()` will be blocked until the `apply()` is not completed.

- **How you load your `Bitmaps`? What do you do for loading large bitmaps?**
[Loading Large Bitmaps Efficiently in Android](https://android.jlelse.eu/loading-large-bitmaps-efficiently-in-android-66826cd4ad53 "Loading Large Bitmaps Efficiently in Android")

- **How Android apps compiled and run?**
  1. First step involves compiling the resources folder (/res) using the aapt
    (android asset packaging tool) tool. These are compiled to a single class
    file called R.java. This is a class that just contains constants.

  2. Second step involves the java source code being compiled to .class files
    by javac, and then the class files are converted to Dalvik bytecode by the
    “dx” tool, which is included in the sdk ‘tools’. The output is classes.dex.

  3. The final step involves the android apkbuilder which takes all the input
    and builds the apk (android packaging key) file.

- **Do you know any about how `Dalvik` is working?**

What is Dalvik?
Programs for Android are commonly written in Java and compiled to bytecode for the Java virtual machine, which is then translated to Dalvik bytecode and stored in .dex (Dalvik EXecutable) and .odex (Optimized Dalvik EXecutable) files;
Dalvik is a Just In Time (JIT) compiler. By the term JIT, we mean to say that whenever you run your app in your mobile device then that part of your code that is needed for execution of your app will only be compiled at that moment and rest of the code will be compiled in the future when needed. 
The JIT or Just In Time compiles only a part of your code and it has a smaller memory footprint and due to this, it uses very less physical space on your device.

What is ART?
ART or Android Runtime is an Android runtime that uses Ahead Of Time(AOT). By using AOT, what is does is it converts or compiles the whole High-level language code
Into Machine level code and at the time of installation of the app and not dynamically as the application runs(like in case of Dalvik). By compiling the whole code during installation results in no lag that we see when we run our app on our device. 
By doing so, the compilation becomes very faster.

More Info about Dalvik And ART (https://blog.mindorks.com/what-are-the-differences-between-dalvik-and-art)

- **What are the benefits of `ART` in comparison to `Dalvik`?**

Difference between ART and Dalvik
Approach: ART uses AOT(Ahead Of Time) approach and compiles the whole code during the installation time but the Dalvik uses JIT(Just In Time) approach and complies only a part of the code during installation and rest of the code will be compiled dynamically.<br>
Booting time: As compare to Dalvik, ART takes more time to reboot because the cache is built at the first time.So, the booting is slow.<br>
Space: Since ART uses the AOT approach, so it needs more space during installation. While Dalvik uses the JIT approach, so for mobile phones having less storage can use the Dalvik.<br>
Battery: ART increases battery performance to a large extent because of the AOT approach. While the Dalvik uses the JIT approach and this results in more battery utilization.<br>
Garbage Collection: ART has better garbage collection than Dalvik.<br>
So, DVM uses JIT and have a lot of drawbacks that were replaced by ART. So, from Android 4.4(Kitkat) ART was introduced as runtime and finally from Android 5.0(Lollipop), the Dalvik was completely replaced by ART by Android.

- **What is `AAPT` ?**

AAPT stands for Android Asset Packaging Tool. The Android Asset Packaging Tool (aapt) takes your application resource files, 
such as the AndroidManifest.xml file and the XML files for your Activities, and compiles them.
This is a great tool which help you to view, create, and update your APKs (as well as zip and jar files).

- **What is Doze mode?**

We have to great power-saving features from Android6(API Level 23):<br>

***Doze*** <br>
***App Standby***<br>

Starting from Android 6.0 (API level 23), Android introduces two power-saving features that extend battery life for users by managing how apps behave when a device is not connected to a power source.<br>
***Doze*** reduces battery consumption by deferring background CPU and network activity for apps when the device is unused for long periods of time.<br> 
***App Standby*** defers background network activity for apps with which the user has not recently interacted.<br>

### Understanding Doze
If a user leaves a device unplugged and stationary for a period of time, with the screen off, the device enters Doze mode. In Doze mode, the system attempts to conserve battery by restricting apps' access to network and CPU-intensive services. It also prevents apps from accessing the network and defers their jobs, syncs, and standard alarms.
But in some period of times the doze let apps to do the reset of their activities!!! but the frequency of this maintenance window will be less and less and ...<br>

***Periodically***, the system exits Doze for a brief time to let apps complete their deferred activities.
During this maintenance window, the system runs all pending syncs, jobs, and alarms, and lets apps access the network.
At the conclusion of each maintenance window, the system again enters Doze, suspending network access and deferring jobs, syncs, and alarms. 
Over time, the system schedules maintenance windows ***less and less frequently***, helping to reduce battery consumption in cases of ***longer-term inactivity*** when the device is not connected to a charger.

(https://developer.android.com/training/monitoring-device-state/doze-standby "More complete Info about Doze ans App Standby")

- **How you can get location in android? What is their differences?**
    
    Basicly there are two ways to get location in the android:
    1. Android’s Location API
    2. Google’s Location Services API
    
    [Complete Answer about getting user location](https://stackoverflow.com/a/33023788/7098269)
