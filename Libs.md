### 6. Tools and libraries

- **What is android DataBinding?**

- **Explain `scope` concept in dagger2**
    Dagger 2 provides @Scope as a mechanism to handle scoping.
    Scoping allows you to �preserve� the object instance and provide it as a �local singleton� for the duration of the scoped component. \
    It means lifecycle of the scoped provider depends to the lifecycle of it's component, Because If you need to scope your provider function, 
    you need to annotate the provider and the it's component __with the same scope annotation__. \
    __@Singleton__, is just another scope that we have it in Dagger as default. It was just there as the default scoping annotation. 
    It provides your dependencies as a singleton as long as you are using __the same component__. \
    the scope is only valid within the component. When a new component of the same type is initialized, it will have a new sets of dependencies. This is the reason why @Singleton components are instantiated and maintained in the Application class. The singleton application component should persist throughout the application lifecycle, 
    and the app should refer to this component to uphold that �single instance� requirement. \
    Following from our architecture design(For example it's Clean Artichecture), we can therefore define each layer as a scope: \   
            ![alt text](https://miro.medium.com/max/954/1*TNcwU_fri3QC_s58yXdpxw.png) \
    More Info about Scope and Component and Subcomponent is [here](https://medium.com/tompee/dagger-2-scopes-and-subcomponents-d54d58511781)

```
    //Sample definition of Scopes in Dagger2
    @Scope
    @Retention
    annotation class MyCustomScope
```


    
- **What is marble diagram?**
- **Explain different types of Observables*
  
    **Single**
        Single is an Observable that always emit only __one value__ or throws __an error__.
        A typical use case of Single observable would be when we make a network call in Android and receive a response.
   
    **Maybe**
        Maybe is an Observable that may or may not emit a value. For example, we would like to know if a particular user exists in our db. 
        The user may or may not exist.
    
    **Completable**
        Completable does not emit any data, but rather is focused on the status of execution � whether successful or failure.
    **Flowable**
        *Back pressure*: I know there should be a 128 item limit on the queue, after that there can be a overflow (backpressure).
        Backpressure is when your observable (publisher) is creating more events than your subscriber can handle.
        So you can get subscribers missing events, or you can get a huge queue of events which just leads to out of memory eventually.
        Flowable takes backpressure into consideration. Observable does not. Thats it.\
        ![alt text](https://i.stack.imgur.com/HjUBs.png)\
        but with using flowable, there is much less backpressure :\
        ![alt text](https://i.stack.imgur.com/L00B6.png)\
        Flowable is typically used when an Observable is emitting huge amounts of data but the Observer is not able to handle this data emission. 
        This is known as Back Pressure.
- **How to implement instant search with RxJava?**
- **What is Subjects in RX?**
    

    We know Observables and Observers. Observables emit data. Observers listen to those emissions by subscribing with the help of Subscribers.
    Subjects can **emit** and **subscribe** to the data. 
    They have the implementations of Observables as well as Observers.
    A Subject has the same operators that an Observable has.
    They can multicast too. This means all the Observers subscribed to it will receive the same emissions from the point of subscription.
    This prevents doing duplicate operations for multiple subscribers.
    
**    Where is a Subject used?**
    Subjects are commonly used when you need to observe some data and then later can emit that data to other observers.

        - **How many Subjects we have in RX?**
            **Behavior**
            **Publish**
            **Reply**
            **Async**
        - **Can you explain their difference?**
        
            All items:   * * * * * 
                            | 

            Publish:         * * * 
            Behaivor:      * * * * 
            Reply:       * * * * * 
            Async:               * 

<br>

[**Good Article about RX Subjects**](https://www.journaldev.com/22573/rxjava-subject)
