### 10. Git

- ***What is GitFlow?***

- ***What is the difference between Merge and Rebase?***
    Merge And Rebase wants to integrate changes from one branch to another branch. But in different ways!!!
    Merge: Merge takes all the changes in one branch and merges them into another branch in one commit. \
        ![alt text](https://cdn-media-1.freecodecamp.org/images/VonhijTBQgjwtRXz31wLzF7iWDnDFk2o8EWi)\
    
    Rebase: Rebase will change the history of logs! When you rebase your branch with another one, Actually you want to move to a new starting point at your branch. /\
        ![alt text](https://cdn-media-1.freecodecamp.org/images/aEjZMJ6s4rDVqzXveqgLrwkQ0RJEvOTjAIUc)\
-***When do you prefer to use Rebase?***\
        -***Merge:*** Let's say you have created a branch for the purpose of developing a single feature. \
        When you want to bring those changes back to master, you probably want merge. \
        -***Rebase:*** A second scenario would be if you started doing some development and then another developer made an unrelated change. 
        You probably want to pull and then rebase to base your changes from the current version from the repository. Thats the place that you need to use Rebase.
