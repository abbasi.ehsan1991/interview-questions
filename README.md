## Android Interview Questions

Before an interview that would be greate to see this [Video](https://www.youtube.com/watch?v=DHDrj0_bMQ0)
    
<br>


#### This document is suitable for you if ...

1. You want to **interview** with a new company.
2. You are working in a company but want to **know** which questions are typically mentioned in interviews.
3. You are an **interviewer** and need some useful questions or want to challenge the interviewee
4. You just want to **improve** your knowledge about android problems and basics.


<br>


| <span style="color:red">*__IMPORTANT NOTICE__*: There is no need to know and read all the questions will be mentioned later. According to the field, size, products and requirements of the company, select and read the appropriate questions. </span>     
| :-------------


<br>

---
#### Table of contents

1. [Object-oriented](https://gitlab.com/abbasi.ehsan1991/interview-questions/-/blob/master/OOP.md "Object-oriented")
2. [Java](https://gitlab.com/abbasi.ehsan1991/interview-questions/-/blob/master/Java.md "Java")
3. [Kotlin](https://gitlab.com/abbasi.ehsan1991/interview-questions/-/blob/master/Kotlin.md "Kotlin")
4. [Android](https://gitlab.com/abbasi.ehsan1991/interview-questions/-/blob/master/Android.md "Android")
5. [Architecture and coding](https://gitlab.com/abbasi.ehsan1991/interview-questions#5-architecture-and-coding "Architecture and coding")
6. [Tools and libraries](https://gitlab.com/abbasi.ehsan1991/interview-questions/-/blob/master/Libs.md "Tools and libraries")
7. [Gradle](https://gitlab.com/abbasi.ehsan1991/interview-questions/-/blob/master/Gradle.md "Gradle")
8. [Design patterns](https://gitlab.com/abbasi.ehsan1991/interview-questions/-/blob/master/Architecture-DesignPatterns.md "Design patterns")
9. [Data structure and algoritms](https://gitlab.com/abbasi.ehsan1991/interview-questions/-/blob/master/Algoritm.md "Data structure and algoritms")
10.[Data structure and algoritms](https://gitlab.com/abbasi.ehsan1991/interview-questions/-/blob/master/GIT.md "Git")

---
<br>

> Experience from one of the Google people:
> My first Software Engineering job search consisted of:

> - ~250+ applications
> - 15 recruiter chats
> - 9 phone interviews
> - 6 onsite interviews
> - 1 offer (from Google)
> 
> There are 3 lessons to take away from this:
> 
> 1) Don’t tie your self-worth to a job search, especially not to your first one.
> 
> 2) All you need is 1 acceptance to completely eclipse hundreds of rejections.
> 
> 3) Rejections and failures are often blessings in disguise, leaving room for better opportunities.

<br>

[TOP 10 TIPS FOR INTERVIEWING YOUR INTERVIEWER](https://www.careertoolbelt.com/top-10-tips-for-interviewing-your-interviewer/ "TOP 10 TIPS FOR INTERVIEWING YOUR INTERVIEWER")
