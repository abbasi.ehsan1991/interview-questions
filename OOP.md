### 1. Object-Oriented

- **What is an Object?**

  An object is an instance of a class that has states and behaviors. A Class
  can be defined as a template that describes the behavior/state that the object
  of its type support.

<br>

- **What is the main feature of OOP ?**

    `Encapsulation`, `Polymorphism`, `Inheritance`, `Abstraction`

<br>

- **What is encapsulation?**

  Encapsulation is one of the four fundamental OOP concepts. It is a mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit. In encapsulation, the variables of a class will be hidden from other classes, and can be accessed only through the methods of their current class. Therefore, it is also known as *data hiding*. To achieve encapsulation in Java:
    - Declare the variables of a class as private.
    - Provide public setter and getter methods to modify and view the variables values.

  Benefits of Encapsulation:
    - The fields of a class can be made read-only or write-only.
    - A class can have total control over what is stored in its fields.

<br>

- **What is the difference between Abstraction and Encapsulation?**

  Even though both Abstraction and Encapsulation looks similar because both hide complexity and make the external interface simpler there is a subtle difference between them. Abstraction hides logical complexity while Encapsulation hides Physical Complexity.

  | Abstraction     | Encapsulation     |
  | :------------- | :------------- |
  | Abstraction solves the problems in the design level       | Encapsulation solves the problems in the implementation level       |
  | Abstraction is used for hiding the unwanted data and giving relevant data | Encapsulation means hiding the code and data into a single unit to protect the data from outside of the world |
  | Abstraction let you focus on what the object does instead of how it does it | Encapsulation means hiding the internal details mechanisms of how an objects does something |
  | Outer layout, used in terms of design: <br> Outer look of mobile phone, like it has display screen and keypad buttons to a number  | Inner layout, used in terms of implementation: <br> For example: Inner implementations detail of a mobile phone, how keypad button and display screen are connect with each other using circuits. |

<br>

- **Difference between abstract and interface?**

  | Interface     | Abstract class     |
  | :------------- | :------------- |
  | Support multiple inheritances | Does not support multiple inheritances |
  | Can extends only from interfaces not classes | Can extends from another class and implement multiple interfaces |
  | Does not contain data member | Contains data member |
  | Does not contains constructors | contains constructors  |
  | In Java Contains only incomplete member (signature of member) | Contains both signature (abstract) of method and member functions |
  | Cannot have access modifiers by default and everything is assumed as public | Can has access modifiers for subs, methods and fields |
  | Cannot have access modifiers by default and everything is assumed as public | Can has access modifiers for subs, methods and fields |
  | They can not save state | Can save state |


  In an interface, you can only define a property without a backing field,
  And **an implementation class** must override that property (with either a backing field or custom accessors).
Given that, you cannot define logic that stores some state in an interface in a reliable way: 
An implementation class might override the properties in an **unexpected** way.

```
interface MyContainer {
    var size: Int

    fun add(item: MyItem) { 
        // ...
        size = size + 1
    }
}
```
Here, we provide a default implementation for add that increments size. But it might break if an implementing class is defined like this:

```
class MyContainerImpl : MyContainer {
    override val size: Int 
        get() = 0
        set(value) { println("Just ignoring the $value") }
}
```

On contrary, abstract classes support this use case and thus allow you to provide some guarantees and contract for all their implementations: 
They can define some state and its transitions that will stay the same in a derived class.



- **What is Polymorphism?**

  The word polymorphism means having many forms. In simple words, In java we have Overridinng and OverLoading for polymorphism.
  
  Overriding:
    It means rewrite the methods from your parent class.
    Happens in Run time
  OverLoading:
    It means have a method with different signatures.
    Happens in compile time
  
- **Can Interfaces to be extended?**

  Yes, an interface can extend other interfaces. it supports multiple
  inheritances, which means it can extend more than one interface. But every
  class which wants to use an interface must add it by keyword `implements`
  and using the keyword `extends` for interfaces in classes is illegal and
  cause compile error.

- **What is the difference between overriding and overloading?**

  | Method Overloading      | Method Overriding     |
  | :-------------   | :------------- |
  | Method overloading is a compile time polymorphism.         | Method overriding is a run time polymorphism.       |
  | It help to rise the readability of the program. | While it is used to grant the specific implementation of the method which is already provided by its parent class or super class. |
  | It is occur within the class.	 | 	While it is performed in two classes with inheritance relationship. |
  | Method overloading may or may not require inheritance. | While method overriding always needs inheritance. |
  | In this, methods must have same name and different signature. | While in this, methods must have same name and same signature. |
  | In method overloading, return type can or can not be be same, but we must have to change the parameter. | While in this, return type must be same or [co-variant](https://typealias.com/guides/illustrated-guide-covariance-contravariance/). |

<br>



<br>
