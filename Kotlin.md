### 3. Kotlin

- **What are the benefits of Kotlin?**
- **What does "Null safety" meaning?**


- **Why Kotlin does not support primitive type?**
Kotlin doesn't have primitive type (I mean you cannot declare primitive directly). It uses classes like `Int`, `Float` as an object wrapper for primitives.
When kotlin code is converted to jvm code, whenever possible, "primitive object" is converted to java primitive. In some cases this cannot be done.
Those cases are, for example, collection of "primitives". For example, `List<Int>` cannot contains primitive. So, compiler knows when it can convert object to primitive. And, again, it's very similar to java:

```
List<Integer> numbers = new ArrayList<>;

numbers.add(0); // <-- you use primitive, but in fact, JVM will convert this primitive to object.
numbers.add(new Integer(0)); // <-- We don't need do that.

```

Also, when you declare "nullable primitive" it is never converted to primitive (what is kind of obvious, as primitive cannot be null).
In java it works very similar:

```
int k = null; // No way!
Integer kN = null; // That's OK.
```

*So, the last conclusion.*
Kotlin doesn't have primitive types out of the box. You treat all objects like objects. Converting to primitive is done at some lower level than code. 
This design is caused to keep compatibility with JVM.

See kotlin under the hood: it's good if you have time :) [Youtube Link](https://www.youtube.com/watch?v=Ta5wBJsC39s)
Another Medium article about this : [Medium Link](https://medium.com/@przemek.materna/kotlin-is-not-primitive-primitives-in-kotlin-and-java-f35713fda5cd)

- **What is the difference between Val and Const in Kotlin? When we need to use const and what is the advantage of it?** <br/>
  [Link to the article: ](https://blog.mindorks.com/what-is-the-difference-between-const-and-val)

- **What is Contracts in Kotlin?**- a <br/>
[Link to great article](https://proandroiddev.com/kotlin-contracts-make-great-deals-with-the-compiler-f524e57f11c)

- **What is Lazy initialization?**
- **What is the data class?**

Kotlin has a better solution for classes that are used to hold data/state. It’s called a Data Class. 
A Data Class is like a regular class but with some additional functionalities.
The compiler automatically generates a default getter and setter for all the mutable properties, and a getter (only) for all the read-only properties of the data class.
Moreover, It also derives the implementation of standard methods like `equals()`, `hashCode()` and `toString()` *from the properties declared in the data class’s primary constructor.*

Data Classes and Immutability: The copy() function:
Although the properties of a data class can be mutable (declared using var), It’s strongly recommended to use immutable properties (declared using val) so as to keep the instances of the data class immutable.

Immutable objects are easier to work with and reason about while working with multi-threaded applications. 
Since they can not be modified after creation, you don’t need to worry about concurrency issues that arise when multiple threads try to modify an object 
At the same time.
Kotlin makes working with immutable data objects easier by automatically generating a copy() function for all the data classes. You can use the copy() function to copy an existing object into a new object and modify some of the properties while keeping the existing object unchanged.   

The following example shows how copy() function can be used:

```
val customer = Customer(3, "James")

/* 
   Copies the customer object into a separate Object and updates the name. 
   The existing customer object remains unchanged.
*/
val updatedCustomer = customer.copy(name = "James Altucher")
println("Customer : $customer")
println("Updated Customer : $updatedCustomer")
```

```
# Output
Customer : Customer(id=3, name=James)
Updated Customer : Customer(id=3, name=James Altucher)

```

Data Classes and **Destructuring Declarations**: The **componentN()** functions:

Kotlin also generates componentN() functions corresponding to all the properties declared in the primary constructor of the data class.
For the Customer data class that we defined in the previous section, Kotlin generates two componentN() functions:
component1() and component2() corresponding to the id and name properties

```
val customer = Customer(4, "Joseph")

println(customer.component1()) // Prints 4
println(customer.component2()) // Prints "Joseph"
```
The Destructuring declaration syntax helps you destructure an object into a number of variables like this:

```
val customer = Customer(4, "Joseph")

// Destructuring Declaration
val (id, name) = customer
println("id = $id, name = $name") // Prints "id = 4, name = Joseph"
```

[Greate Article for Data Classes](https://www.callicoder.com/kotlin-data-classes/)


- **What is Destructuring Declarations in Kotlin? How it cloud be possible to that?**

    A destructuring declaration creates multiple variables at once & It could be happen by Data classes because of generation componentN() for each property 
    In the primary constructor.

Sometimes it is convenient to destructure an object into a number of variables, for example:


`val (name, age) = person`

This syntax is called a destructuring declaration. A destructuring declaration creates multiple variables at once. We have declared two new variables: name and age, and can use them independently:

```
println(name)
println(age
```

A destructuring declaration is compiled down to the following code:

```
val name = person.component1()
val age = person.component2()
```


Example: Returning Two Values from a Function

Let's say we need to return two things from a function. For example, a result object and a status of some sort. 
A compact way of doing this in Kotlin is to **declare a data class** and return its instance:

```
data class Result(val result: Int, val status: Status)
fun function(...): Result {
    // computations
    
    return Result(result, status)
}

// Now, to use this function:
val (result, status) = function(...)
```

Since data classes automatically declare componentN() functions, destructuring declarations work here.

    NOTE: we could also use the standard class Pair and have function() return Pair<Int, Status>, 
    But it's often better to have your data named properly.


- **Is it possible to inherit a class/method/property by default?**
- **Difference between apply, also?**
- **Difference between let, also?**

- **Difference between let, also, Apply, Run, With?**
    
    
```
inline fun <T, R> with(receiver: T, block: T.() -> R): R {
        return receiver.block()
    }
```

```
    inline fun <T> T.also(block: (T) -> Unit): T {
        block(this)
        return this
    }
```

```
    inline fun <T> T.apply(block: T.() -> Unit): T {
        block()
        return this
    }
```

```
    inline fun <T, R> T.let(block: (T) -> R): R {
        return block(this)
    }
```

```
    inline fun <T, R> T.run(block: T.() -> R): R {
        return block()
    }
```

("https://www.journaldev.com/19467/kotlin-let-run-also-apply-with")

- **What `==` exactly do in Kotlin in comparison to Java?**- 

  `==`operator is used to compare the data of two variables in Kotlin.
  Please don’t misunderstand this equality operator with the Java `==` operator as both are different.
  `==`operator in Kotlin only compares the data or variables, whereas in Java or other languages `==` is generally used to compare the references(Memory location).


- **What is the difference between parameter and argument?**
- **What is the difference between function and method?**
- **What is the "receiver" in the extension function?**
- **What is operator overloading?**
- **Is it possible to write a static method as java as has?**
- **What is a sealed class?**
- **How does an extension function work?**
- **What is `reified` keyword?**
- **What is an inline function?**
- **What are the cons of using an inline function?**
- **What is the best practice of using an inline function?**
- **How does the `companion object` block work?**
- **Is it possible to create an extension function on the `companion object` of a class?**
- **Extension function as a member, possible? What are the benefits of declaring an extension function as a member?**
- **What is a spread operator? What is the recommended place to use it?**
- **What is the producer’s function? how to demonstrate it in Kotlin?**
- **What is the consumer’s function? how to demonstrate it in Kotlin?**
- **What is the higher-order function?**
- **How to compare two Strings in Kotlin?**
- **What is the difference between `==` and `===` ?**

<br>

