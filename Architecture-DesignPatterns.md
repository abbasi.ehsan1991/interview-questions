**Facade  Design pattern:**


**Category**: Structural 

**Why we need it?** 

It helps us to simply usage of some related subsystems. With a facade( A class that it's role is a wrapper for our subsystems. )
We can make it easy for the client to use of related classes, With a simple wrapper.

**How it works? **
    
There is a good eample of implementation for this pattern. (Greate example, https://medium.com/@andreaspoyias/design-patterns-a-quick-guide-to-facade-pattern-16e3d2f1bfb6)
It's not complicated, We need to create a class that is our Facad, Then there will be method(s) that client needs to see and use them.

    Subsystems are hidden from the client.
    
**By the way what's the difference between Facade  and Factory?**

*Factory* is a creational pattern, And it's mission is to hide complexity of creating some objects that they are from a super type.

*Facade* is a Structional pattern, And it's mission is to hide complexity of relation between subsystems(Subsystems are related together).


